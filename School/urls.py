from django.contrib import admin
from django.urls import path
from Academic import views
urlpatterns = [
    path('', views.index, name='index'),
    path('delete/', views.delete, name='delete'),
    path('update/', views.update, name='update'),
    path('admin/', admin.site.urls),
]
