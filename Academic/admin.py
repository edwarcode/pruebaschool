from django.contrib import admin
from .models import *

admin.site.register(Rol)
admin.site.register(Jornada)
admin.site.register(Personal)
admin.site.register(Grado)
