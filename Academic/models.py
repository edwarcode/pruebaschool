from django.db import models


class Rol(models.Model):
    rol = models.CharField(max_length=140)

    def __str__(self):
        return "%s" % (self.rol)


class Jornada(models.Model):
    jornada = models.CharField(max_length=5)

    def __str__(self):
        return "%s" % (self.jornada)


class Personal(models.Model):
    apellidos = models.CharField(max_length=200)
    nombres = models.CharField(max_length=140)
    tipo_documento_personal = models.CharField(max_length=5)
    documento_personal = models.BigIntegerField()
    titulo_personal = models.CharField(max_length=200)
    email = models.EmailField(max_length=200)
    telefonoCell = models.CharField(max_length=15)
    rol = models.ForeignKey(Rol, on_delete=models.CASCADE,)
    jornada = models.ForeignKey(Jornada, on_delete=models.CASCADE,)

    class Meta:
        unique_together = (('email',),)

    def save(self, force_insert=False, force_update=False):
        self.apellidos = self.apellidos.upper()
        self.nombres = self.nombres.upper()
        super(Personal, self).save(force_insert, force_update)

    def __str__(self):
        return "%s - %s" % (self.nombres, self.apellidos)


class Grado(models.Model):
    grado = models.CharField(max_length=200)
    jornada = models.ForeignKey(Jornada, on_delete=models.CASCADE,)
    personal = models.ManyToManyField(Personal)

    def __str__(self):
        return"%s" % (self.grado)
