from django.shortcuts import render, redirect, get_object_or_404
from .models import Personal, Rol, Jornada
from .forms import PersonalForm
from django.views.generic import ListView, DetailView
from django.template.context import RequestContext
from django.contrib import messages
from django.http import JsonResponse, HttpResponseRedirect


def index(request):
    template_name = 'index.html'
    context_object_name = 'contact_list'
    if request.method == 'POST':
        fromPersonal = PersonalForm(request.POST)
        if fromPersonal.is_valid():
            fromPersonal.save()
            messages.success(request, 'registro Exitoso !!',
                             extra_tags='alert alert-success')
        else:
            messages.warning(request, 'Debe completar los campos',
                             extra_tags='alert alert-danger')

    fromPesonal = PersonalForm()
    dataPersonal = Personal.objects.all()
    rols = Rol.objects.all()
    jornadas = Jornada.objects.all()
    return render(request, template_name, locals())


def update(request):
    if request.method == 'POST':
        personal = Personal.objects.get(pk=request.POST['pk'])
        personal.nombres = request.POST['nombres']
        personal.apellidos = request.POST['apellidos']
        personal.tipo_documento_personal = request.POST['tipo_documento_personal']
        personal.documento_personal = request.POST['documento_personal']
        personal.titulo_personal = request.POST['titulo_personal']
        personal.email = request.POST['email']
        personal.telefonoCell = request.POST['telefonoCell']
        personal.rol = request.POST['rol']
        personal.jornada = request.POST['jornada']
        personal.save()
        messages.success(request, 'registro actualizado Exitoso !!',
                         extra_tags='alert alert-success')
    return HttpResponseRedirect("/")


def delete(request):
    if request.method == 'POST':
        template_name = 'index.html'
        context_object_name = 'contact_list'
        personal = Personal.objects.get(pk=request.POST['pk'])
        if personal:
            personal.delete()
        else:
            messages.warning(request, 'Id no encontrado',
                             extra_tags='alert alert-danger')
        fromPesonal = PersonalForm()
        dataPersonal = Personal.objects.all()
        messages.success(request, 'registro Eliminado !!',
                         extra_tags='alert alert-success')
        return HttpResponseRedirect("/")
